#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'java'
require 'bouncycastle'

# rubocop: disable Style/MethodCallWithoutArgsParentheses
# rubocop: disable Style/StringConcatenation
# module SSL
module SSL
  # module InternalInstanceMethods
  module InternalInstanceMethods
    DEFAULT_FQDN = 'localhost'.freeze
    DEFAULTS = {
      algorithm: 'RSA',
      bits: 4096,
      not_before: java.util.Date.new(java.lang.System.currentTimeMillis() - (86_400_000 * 365)),
      not_after: java.util.Date.new(253_402_300_799_000)
    }.freeze
    attr_reader :certificate, :private_key, :key, :cert

    def keypair_generator(algorithm)
      java.security.KeyPairGenerator.getInstance(algorithm)
    rescue java.security.NoSuchAlgorithmException => e
      # Should not reach here because every Java implementation must
      # have RSA and EC key pair generator.
      warn "FATAL: #{e.message}"
      exit
    end

    def initialize_keypair_generator(generator, bits, random)
      generator.java_method(
        :initialize, [Java::int, java.security.SecureRandom]
      ).call(bits, random)
      generator
    end

    def generate_keypair(algorithm, bits, random)
      generator = keypair_generator(algorithm)
      initialize_keypair_generator(generator, bits, random).generateKeyPair()
    end

    def write_pem(type, data, buffer = java.io.StringWriter.new)
      writer = org.bouncycastle.util.io.pem.PemWriter.new(buffer)
      writer.writeObject(org.bouncycastle.util.io.pem.PemObject.new(type, data))
      buffer
    ensure
      writer&.close
    end

    def save_pem(type, data, filename)
      file_path = File.join(__dir__, filename)
      IO.write(File.new(file_path, 'w'), write_pem(type, data).to_s)
      file_path
    end

    def owner
      @owner ||= Java::org.bouncycastle.asn1.x500.X500Name.new('CN=' + @fqdn)
    end

    # Prepare the information required for generating an X.509 certificate.
    def certificate_builder(random, options)
      Java::org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder.new(
        owner,
        java.math.BigInteger.new(64, random),
        options[:not_before],
        options[:not_after],
        owner,
        @public_key
      )
    end

    def signer_builder(type = 'SHA256WithRSAEncryption')
      @signer_builder ||= Java::org.bouncycastle.operator.jcajce.JcaContentSignerBuilder.new(type)
    end

    def signer
      @signer ||= signer_builder.build(@private_key)
    end

    def converter
      @converter ||= Java::org.bouncycastle.cert.jcajce.JcaX509CertificateConverter.new
    end

    def provider
      @provider ||= Java::org.bouncycastle.jce.provider.BouncyCastleProvider.new
    end

    def signed_certificate(random, options)
      @signed_certificate ||= certificate_builder(random, options).build(signer)
    end

    def generate_certificate(random, options)
      converter.setProvider(provider)
      cert = converter.getCertificate(signed_certificate(random, options))
      cert.verify(@public_key)
      cert
    rescue StandardError => e
      warn 'FATAL: Failed to generate a self-signed X.509 certificate using Bouncy Castle:'
      warn e.message
      e.backtrace.each { |t| warn t }
    end
  end
  # module InternalInstanceMethods

  # module InstanceMethods
  module InstanceMethods
    def load_certificate(certificate_pem = @certificate)
      stream = File.new(certificate_pem, 'r').to_input_stream
      certificate_factory = java.security.cert.CertificateFactory.getInstance('X509')
      certificate_factory.generateCertificate(stream)
    ensure
      begin
        stream&.close
      rescue StandardError => e
        warn "WARN: Failed to close a file: #{certificate_pem}: #{e.message}"
      end
    end

    def save
      # Change all asterisk to 'x' for file name safety.
      @fqdn.gsub!(/\*/, 'x')
      # Encode the private key and write it to a file
      @key = save_pem('RSA PRIVATE KEY', @private_key.getEncoded(), @fqdn + '.key')
      # Encode the certificate and write it to a file
      @certificate = save_pem('CERTIFICATE', @cert.getEncoded(), @fqdn + '.crt')
    end
  end
  # module InstanceMethods

  class SelfSignedCertificate
    include InstanceMethods
    def initialize(fqdn = DEFAULT_FQDN.dup, options = {})
      options = DEFAULTS.merge(options)
      @fqdn = fqdn
      random = java.security.SecureRandom.new
      keypair = generate_keypair(options[:algorithm], options[:bits], random)
      @private_key = keypair.getPrivate()
      @public_key = keypair.getPublic()
      @cert = generate_certificate(random, options)
    end

    include InternalInstanceMethods
  end
end
# module SSL
# rubocop: enable Style/MethodCallWithoutArgsParentheses
# rubocop: enable Style/StringConcatenation

# module Example
module Example
  def main(_args = ARGV)
    self_signed_certificate = SSL::SelfSignedCertificate.new
    cert = self_signed_certificate.save
    puts self_signed_certificate.load_certificate(cert)
    File.delete(self_signed_certificate.key)
    File.delete(self_signed_certificate.certificate)
  end
end
# module Example

Object.new.extend(Example).main if $PROGRAM_NAME == __FILE__
