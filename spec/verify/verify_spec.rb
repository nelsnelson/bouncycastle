# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

unless defined?(PROJECT)
  VERIFY_DIR_PATH = File.expand_path(__dir__) unless defined?(VERIFY_DIR_PATH)
  SPEC_DIR_PATH = File.expand_path(File.dirname(VERIFY_DIR_PATH)) unless defined?(SPEC_DIR_PATH)
  PROJECT_DIR_PATH = File.expand_path(File.dirname(SPEC_DIR_PATH)) unless defined?(PROJECT_DIR_PATH)
  PROJECT = File.basename(PROJECT_DIR_PATH)
end

RSpec.describe 'verify gem' do
  it 'should support generating a self-signed certificate' do
    require 'fileutils'
    load "#{PROJECT}.gemspec"
    spec_verify_path = File.expand_path(__dir__)
    spec_path = File.expand_path(File.dirname(spec_verify_path))
    project_path = File.expand_path(File.dirname(spec_path))
    gem_spec_full_name = GEM_SPEC.full_name
    gem_file_name = gem_spec_full_name + '.gem'
    gem_path = File.join(project_path, gem_file_name)
    FileUtils.mkdir_p('tmp')
    system('jgem', 'install', '--install-dir=tmp', gem_path)
    gem_lib_dir_path = File.join('tmp', 'gems', gem_spec_full_name, 'lib')
    verify_rb_file_path = File.join('spec', 'verify', 'example.rb')
    command = [
      'jruby', "-I#{gem_lib_dir_path}", '-rbouncycastle', verify_rb_file_path, #'2>/dev/null',
    ]
    cmd = command.join ' '
    puts "Executing command: #{cmd}"
    results = `#{cmd}`.strip
    expected_subject = %r/Subject: CN=localhost/
    expected_issuer = %r/Issuer: CN=localhost/
    expect {
      results =~ expected_subject && results =~ expected_issuer
    }
  end
end
