# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

module BouncyCastle
  def self.version
    File.read('version').chomp
  end

  def self.release
    File.read('release').chomp
  end

  def self.release_version(release = BouncyCastle.release)
    release_version = [BouncyCastle.version]
    release_version << release unless release.nil?
    release_version.join('.').freeze
  end

  VERSION = BouncyCastle.version
  GEM_RELEASE = BouncyCastle.release
end
