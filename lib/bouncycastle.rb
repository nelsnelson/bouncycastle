# encoding: utf-8

begin
  require 'java'

  gem_path = File.expand_path(File.dirname(__FILE__))

  Dir[File.join(gem_path, '**/*.jar')].each do |jar|
    begin
      require jar
    rescue LoadError => e
      STDERR.puts "Failed to load jar: #{e.message}"
    end
  end
rescue LoadError => e
  STDERR.puts "No java support: #{e.message}"
rescue StandardError => e
  STDERR.puts "Unexpected error: #{e.message}"
end
